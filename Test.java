package com.company;

import java.util.List;
import java.util.Vector;

/**
 * This is the testing class
 */
public class Test {
    /**
     * A method to run all my tests
     */
    public void run_tests(){
        get_piece_name_test();
    }

    /**
     * A method to test the get_piece_name method
     */
    public void get_piece_name_test(){
        Game test_game = new Game();
        assert (test_game.get_piece_name(1, 6) == "P");
        assert (test_game.get_piece_name(0, 4) == "K");
        assert (test_game.get_piece_name(7, 6) == "R");
    }

    /**
     * A method to test the make_move method
     */
    public void make_move_test() throws InterruptedException {
        Game test_game = new Game();
        test_game.initialize_board();
        assert (test_game.make_move() == -1);
    }

    /**
     * A method to test the pick_move method
     */
    public void pick_move_test() throws InterruptedException {
        Game test_game = new Game();
        test_game.initialize_board();
        assert (test_game.make_move() == -1);
    }

    /**
     * A method to test the legal_square_addition method
     */
    /*
    public void legal_square_addition_test() throws InterruptedException {
        Piece test_piece = new Piece();
        List<int[]> squares = new Vector<int[]>();
        int[] square = new int[2];
        square[0] = 2;
        square[1] = 3;
        squares.add(square);
        assert(test_piece.legal_square_addition(2, 3) == squares);
    }
    */


}
