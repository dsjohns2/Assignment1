package com.company;

import java.util.Arrays;
import java.util.List;
import java.util.Vector;

/**
 * This class is what makes up the game board.
 */
public class Square {
    public boolean occupied;
    public String name;
    public boolean white;
    public int x;
    public int y;

    /**
     * A default constructor
     */
    public Square(){
        this.occupied = false;
        this.name = "";
        this.white = true;
        this.x = -1;
        this.y = -1;
    }

    /**
     * A copy constructor
     * @param other the other square to copy
     */
    public Square(Square other){
        this.occupied = other.occupied;
        this.name = other.name;
        this.white = other.white;
        this.x = other.x;
        this.y = other.y;
    }

    /**
     * test if two squares are equal
     * @param square1 first board
     * @param square2 second board
     * @return true if equal, false if not
     */
    public boolean equal_squares(Square square1, Square square2){
        if(square1.occupied && square2.occupied){
            if((square1.name == square2.name) && (square1.white == square2.white)){
                return true;
            }
            else{
                return false;
            }
        }
        else if(!square1.occupied && !square2.occupied){
            return true;
        }
        else{
            return false;
        }
    }

    /**
     * A method to return the possible locations the current piece can reach given its current location
     * @param board the current state of the board
     * @return an array of tuples representing x and y, the locations that can be reached
     */
    public int[][] squares_that_can_be_taken(Square[][] board){
        if(this.name == "P" && this.white){
            return white_pawn_squares(board);
        }
        else if(this.name == "P" && !this.white){
            return black_pawn_squares(board);
        }
        else if(this.name == "R"){
            return rook_squares(board);
        }
        else if(this.name == "N"){
            return knight_squares(board);
        }
        else if(this.name == "B"){
            return bishop_squares(board);
        }
        else if(this.name == "Q"){
            return queen_squares(board);
        }
        else if(this.name == "A"){
            return A_squares(board);
        }
        else if(this.name == "Z"){
            return Z_squares(board);
        }
        else{
            return king_squares(board);
        }
    }

    /**
     * A helper method returning the possible locations of a white pawn
     * @param board the current state of the board
     * @return an array of tuples representing x and y, the locations that can be reached
     */
    public int[][] white_pawn_squares(Square[][] board){
        List<int[]> squares = new Vector<int[]>();
        int x = this.x;
        int y = this.y;

        /* move forward one */
        if(x-1>=0 && !board[x-1][y].occupied){
            squares.addAll(legal_square_addition(x-1, y));
        }
        /* move forward two */
        if(x==6 && !board[x-1][y].occupied && !board[x-2][y].occupied){
            squares.addAll(legal_square_addition(x-2, y));
        }
        /* take piece left */
        if(x-1 >= 0 && y-1 >= 0 && board[x-1][y-1].occupied && !board[x-1][y-1].white){
            squares.addAll(legal_square_addition(x-1, y-1));
        }
        /* take piece right */
        if(x-1 >= 0 && y+1 <= 7 && board[x-1][y+1].occupied && !board[x-1][y+1].white){
            squares.addAll(legal_square_addition(x-1, y+1));
        }
        return squares.toArray(new int[squares.size()][2]);
    }

    /**
     * A helper method to add legal squares to the square list
     * @param new_x the x coord
     * @param new_y the y coord
     * @return the list of legal square tuples
     */
    public List<int[]> legal_square_addition(int new_x, int new_y){
        List<int[]> squares = new Vector<int[]>();
        int[] square = new int[2];
        square[0] = new_x;
        square[1] = new_y;
        squares.add(square);
        return squares;
    }

    /**
     * A helper method returning the possible locations of a black pawn
     * @param board the current state of the board
     * @return an array of tuples representing x and y, the locations that can be reached
     */
    public int[][] black_pawn_squares(Square[][] board){
        List<int[]> squares = new Vector<int[]>();
        int x = this.x;
        int y = this.y;

        /* move forward one */
        if(x+1<=7 && !board[x+1][y].occupied){
            squares.addAll(legal_square_addition(x+1, y));
        }
        /* move forward two */
        if(x==1 && !board[x+1][y].occupied && !board[x+2][y].occupied){
            squares.addAll(legal_square_addition(x+2, y));
        }
        /* take piece left */
        if(x+1 <= 7 && y-1 >= 0 && board[x+1][y-1].occupied && board[x+1][y-1].white){
            squares.addAll(legal_square_addition(x+1, y-1));
        }
        /* take piece right */
        if(x+1 <= 7 && y+1 <= 7 && board[x+1][y+1].occupied && board[x+1][y+1].white){
            squares.addAll(legal_square_addition(x+1, y+1));
        }
        return squares.toArray(new int[squares.size()][2]);
    }

    /**
     * A method to determine the possible square locations in a straight line from x, y
     * @param board the current state of the board
     * @param x_inc the x direction of the line
     * @param y_inc the y direction of the line
     * @return a list of tuples representing x and y, the locations that can be reached
     */
    public List<int[]> line(Square[][] board, int x_inc, int y_inc){
        List<int[]> squares = new Vector<int[]>();
        int x = this.x;
        int y = this.y;

        int new_x = x+x_inc;
        int new_y = y+y_inc;
        while(new_x>=0 && new_x<=7 && new_y>=0 && new_y<=7){
            if(board[new_x][new_y].occupied){
                if(board[new_x][new_y].white != board[x][y].white){
                    int[] square = new int[2];
                    square[0] = new_x;
                    square[1] = new_y;
                    squares.add(square);
                }
                break;
            }
            else{
                int[] square = new int[2];
                square[0] = new_x;
                square[1] = new_y;
                squares.add(square);
                new_x = new_x+x_inc;
                new_y = new_y+y_inc;
            }
        }
        return squares;
    }

    /**
     * A helper method returning the possible locations of a rook
     * @param board the current state of the board
     * @return an array of tuples representing x and y, the locations that can be reached
     */
    public int[][] rook_squares(Square[][] board){
        List<int[]> squares = new Vector<int[]>();

        /* Up */
        squares.addAll(line(board, -1, 0));
        /* Down */
        squares.addAll(line(board, 1, 0));
        /* Left */
        squares.addAll(line(board, 0, -1));
        /* Right */
        squares.addAll(line(board, 0, 1));
        return squares.toArray(new int[squares.size()][2]);
    }

    /**
     * A helper method returning the possible locations of a bishop
     * @param board the current state of the board
     * @return an array of tuples representing x and y, the locations that can be reached
     */
    public int[][] bishop_squares(Square[][] board) {
        List<int[]> squares = new Vector<int[]>();

        /* Up - Left */
        squares.addAll(line(board, -1, -1));
        /* Up - Right */
        squares.addAll(line(board, -1, 1));
        /* Down - Left */
        squares.addAll(line(board, 1, -1));
        /* Down - Right */
        squares.addAll(line(board, 1, 1));
        return squares.toArray(new int[squares.size()][2]);
    }

    /**
     * A method to check if the current piece can reach new_x, new_y from x, y
     * @param board the current state of the board
     * @param x current x location
     * @param y current y location
     * @param new_x target x location
     * @param new_y target y location
     * @return A list of tuples representing x and y, the locations that can be reached
     */
    public List<int[]> square_checker(Square[][] board, int x, int y, int new_x, int new_y){
        List<int[]> squares = new Vector<int[]>();
        if(new_x >= 0 && new_x <= 7 && new_y >= 0 && new_y <= 7){
            if(!board[new_x][new_y].occupied || board[x][y].white != board[new_x][new_y].white) {
                int[] square = new int[2];
                square[0] = new_x;
                square[1] = new_y;
                squares.add(square);
            }
        }
        return squares;
    }

    /**
     * A helper method returning the possible locations of a knight
     * @param board the current state of the board
     * @return an array of tuples representing x and y, the locations that can be reached
     */
    public int[][] knight_squares(Square[][] board) {
        List<int[]> squares = new Vector<int[]>();
        int x = this.x;
        int y = this.y;
        squares.addAll(square_checker(board, x, y, x-1, y-2));
        squares.addAll(square_checker(board, x, y, x-1, y+2));
        squares.addAll(square_checker(board, x, y, x-2, y-1));
        squares.addAll(square_checker(board, x, y, x-2, y+1));
        squares.addAll(square_checker(board, x, y, x+1, y-2));
        squares.addAll(square_checker(board, x, y, x+1, y+2));
        squares.addAll(square_checker(board, x, y, x+2, y-1));
        squares.addAll(square_checker(board, x, y, x+2, y+1));
        return squares.toArray(new int[squares.size()][2]);
    }

    /**
     * A helper method returning the possible locations of a queen
     * @param board the current state of the board
     * @return an array of tuples representing x and y, the locations that can be reached
     */
    public int[][] queen_squares(Square[][] board) {
        List<int[]> squares = new Vector<int[]>();
        squares.addAll(Arrays.asList(bishop_squares(board)));
        squares.addAll(Arrays.asList(rook_squares(board)));
        return squares.toArray(new int[squares.size()][2]);
    }

    /**
     * A helper method returning the possible locations of A piece
     * @param board the current state of the board
     * @return an array of tuples representing x and y, the locations that can be reached
     */
    public int[][] A_squares(Square[][] board) {
        List<int[]> squares = new Vector<int[]>();
        int x = this.x;
        int y = this.y;
        for(int i=0; i<=7; i++){
            for(int j=0; j<=7; j++){
                if(!(i==x && j==y) && board[i][j].name != "K" && board[i][j].name != "A") {
                    squares.addAll(square_checker(board, x, y, i, j));
                }
            }
        }
        return squares.toArray(new int[squares.size()][2]);
    }

    /**
     * A helper method returning the possible locations of B piece
     * @param board the current state of the board
     * @return an array of tuples representing x and y, the locations that can be reached
     */
    public int[][] Z_squares(Square[][] board) {
        List<int[]> squares = new Vector<int[]>();
        int x = this.x;
        int y = this.y;
        if(x+y % 2 == 0) {
            squares.addAll(Arrays.asList(bishop_squares(board)));
        }
        else {
            squares.addAll(Arrays.asList(rook_squares(board)));
        }
        return squares.toArray(new int[squares.size()][2]);
    }

    /**
     * A helper method returning the possible locations of a king
     * @param board the current state of the board
     * @return an array of tuples representing x and y, the locations that can be reached
     */
    public int[][] king_squares(Square[][] board) {
        List<int[]> squares = new Vector<int[]>();
        int x = this.x;
        int y = this.y;
        squares.addAll(square_checker(board, x, y, x-1, y-1));
        squares.addAll(square_checker(board, x, y, x-1, y+1));
        squares.addAll(square_checker(board, x, y, x, y-1));
        squares.addAll(square_checker(board, x, y, x, y+1));
        squares.addAll(square_checker(board, x, y, x+1, y-1));
        squares.addAll(square_checker(board, x, y, x+1, y+1));
        squares.addAll(square_checker(board, x, y, x-1, y));
        squares.addAll(square_checker(board, x, y, x+1, y));
        return squares.toArray(new int[squares.size()][2]);
    }
}
