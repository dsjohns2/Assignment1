package com.company;

import java.util.List;
import java.util.Random;
import java.util.Vector;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

/**
 * The class that contains the entire chess game
 */
public class Game {
    public int white_score = 0;
    public int black_score = 0;
    public int x_dim = 8;
    public int y_dim = 8;
    public Square[][] board = new Square[x_dim][y_dim];
    public Square[][] last_position = new Square[x_dim][y_dim];
    public boolean white_turn = true;

    /**
     * A default constructor
     */
    public Game(){
    }

    /**
     * A copy constructor
     * @param other the other game
     */
    public Game(Game other){
        this.x_dim = other.x_dim;
        this.y_dim = other.y_dim;
        this.white_turn = other.white_turn;
        for(int i=0; i<x_dim; i++){
            for(int j=0; j<y_dim; j++){
                this.board[i][j] = new Square(other.board[i][j]);
                this.last_position[i][j] = new Square(other.last_position[i][j]);
            }
        }
    }

    /**
     * This method sets up the game board at the beginning of the game.
     */
    public void initialize_board(){
        this.white_turn = true;
        for(int i=0; i<x_dim; i++){
            for(int j=0; j<y_dim; j++){
                Square new_square = new Square();
                if(i==0 || i==1 | i==6 | i==7){
                    new_square.occupied = true;
                    new_square.name = get_piece_name(i, j);
                    new_square.x = i;
                    new_square.y = j;
                    if(i<2){
                        new_square.white = false;
                    }
                    else {
                        new_square.white = true;
                    }
                }
                else{
                    new_square.occupied = false;
                }
                board[i][j] = new_square;
                last_position[i][j] = new Square(new_square);
            }
        }
        add_piece_to_location(2, 3, "A", false, true);
        add_piece_to_location(2, 4, "Z", false, true);
        add_piece_to_location(5, 3, "A", true, true);
        add_piece_to_location(5, 4, "Z", true, true);
    }

    /**
     * A helper method to add pieces to whereever on the board; useful for debugging
     * @param x the x coord
     * @param y the y coord
     * @param name the name of the piece
     * @param white whose turn
     * @param occupied whether the square should be occupied
     */
    public void add_piece_to_location(int x, int y, String name, boolean white, boolean occupied){
        Square new_square = new Square();
        new_square.occupied = occupied;
        new_square.name = name;
        new_square.white = white;
        new_square.x = x;
        new_square.y = y;
        this.board[x][y] = new_square;
    }

    /**
     * A method to determine the piece name during initialization
     * @param i the x location
     * @param j the y location
     * @return the name of the piece
     */
    public String get_piece_name(int i, int j){
        if(i==1 || i==6) {
            return "P";
        }
        else if(j==0 || j==7) {
            return "R";
        }
        else if(j==1 || j==6){
            return "N";
        }
        else if(j==2 || j==5){
            return "B";
        }
        else if(j==3){
            return "Q";
        }
        else{
            return "K";
        }
    }

    /**
     * A method to change board states through a legal move
     * @return  -1 if the game is not over
     *          0 if the game is a stalemate
     *          1 if white has won
     *          2 if black has won     *
     * @throws InterruptedException
     */
    public int make_move() throws InterruptedException {
        Square[][][] legal_moves = get_all_moves(white_turn, this.board);

        /* Check for end of game */
        int end_val = check_for_end_of_game(legal_moves);
        if(end_val>0){
            return end_val;
        }

        /* Game continues */
        this.board = pick_move(legal_moves);
        white_turn = !white_turn;
        return -1;
    }

    public int check_for_end_of_game(Square[][][] legal_moves){
        if(legal_moves.length == 0){
            boolean stalemate_flag = true;
            List<Square[][]> next_move_legal_moves = get_legal_moves(!white_turn, this.board);
            for(int j=0; j<next_move_legal_moves.size(); j++) {
                boolean king_found = false;
                for (int k = 0; k < x_dim; k++) {
                    for (int l = 0; l < y_dim; l++) {
                        if(next_move_legal_moves.get(j)[k][l].occupied && next_move_legal_moves.get(j)[k][l].name == "K" && next_move_legal_moves.get(j)[k][l].white == white_turn){
                            king_found = true;
                        }
                    }
                }
                if(!king_found){
                    stalemate_flag = false;
                    break;
                }
            }
            if(stalemate_flag){
                return 0;
            }
            else if(!white_turn){
                return 1;
            }
            else{
                return 2;
            }
        }
        return -1;
    }

    /**
     * A method to pick one of the valid moves to make
     * @param legal_moves the array of valid moves to choose from
     * @return a legal move
     */
    public Square[][] pick_move(Square[][][] legal_moves){
        if(legal_moves.length == 1){
            return(legal_moves[0]);
        }
        Random rand = new Random();
        int n = rand.nextInt(legal_moves.length-1);
        return(legal_moves[n]);
    }

    /**
     * test if two boards are equal
     * @param board1 first board
     * @param board2 second board
     * @return true if equal, false if not
     */
    public boolean equal_boards(Square[][] board1, Square[][] board2){
        boolean is_equal = true;
        for(int i=0; i<x_dim; i++) {
            for (int j = 0; j < y_dim; j++) {
                if(!board1[i][j].equal_squares(board1[i][j], board2[i][j])){
                    is_equal = false;
                    break;
                }
            }
        }
        return is_equal;
    }

    /**
     * A method to get the legal moves and then remove the moves that end with the play still in check
     * @param white whose turn
     * @param board the state of the board
     * @return the list of truely legal moves
     */
    public Square[][][] get_all_moves(boolean white, Square[][] board){
        List<Square[][]> legal_moves = get_legal_moves(white, board);
        Square[][][] new_legal_moves = remove_moves_into_check(white, legal_moves);
        return new_legal_moves;
    }
    /**
     * A method to get all of the legal moves available to the player about to move
     * @param white the player about to move
     * @return an array of legal moves/board states
     */
    public List<Square[][]> get_legal_moves(boolean white, Square[][] board){
        List<Square[][]> legal_moves = new Vector<Square[][]>();
        for(int i=0; i<x_dim; i++) {
            for (int j = 0; j < y_dim; j++) {
                if (board[i][j].occupied && board[i][j].white == white) {
                    int[][] squares = board[i][j].squares_that_can_be_taken(board);
                    for (int k = 0; k < squares.length; k++) {
                        /* copy board */
                        Square[][] new_board = new Square[board.length][8];
                        for (int row = 0; row < board.length; row++) {
                            for (int col = 0; col < new_board.length; col++) {
                                new_board[row][col] = new Square(board[row][col]);
                            }
                        }
                        /* add new piece */
                        new_board[squares[k][0]][squares[k][1]] = new Square(new_board[i][j]);
                        new_board[squares[k][0]][squares[k][1]].x = squares[k][0];
                        new_board[squares[k][0]][squares[k][1]].y = squares[k][1];
                        /* remove old piece */
                        new_board[i][j].occupied = false;
                        /* add new board state to list */
                        legal_moves.add(new_board);
                    }
                }
            }
        }
        return legal_moves;
    }

    /**
     * A method to remove the legal moves ending in check
     * @param legal_moves the "legal moves" coming in
     * @return the legal moves that don't end with check
     */
    public Square [][][] remove_moves_into_check(boolean white, List<Square[][]> legal_moves){
        for(int i=0; i<legal_moves.size(); i++){
            List<Square[][]> next_move_legal_moves = get_legal_moves(!white, legal_moves.get(i));
            for(int j=0; j<next_move_legal_moves.size(); j++) {
                boolean king_found = false;
                for (int k = 0; k < x_dim; k++) {
                    for (int l = 0; l < y_dim; l++) {
                        if(next_move_legal_moves.get(j)[k][l].occupied && next_move_legal_moves.get(j)[k][l].name == "K" && next_move_legal_moves.get(j)[k][l].white == white){
                            king_found = true;
                        }
                    }
                }
                if(!king_found){
                    legal_moves.set(i, null);
                    break;
                }
            }
        }
        int max_legal_moves_size = legal_moves.size();
        for(int i=0; i<max_legal_moves_size; i++){
            legal_moves.remove(null);
        }
        return legal_moves.toArray(new Square[legal_moves.size()][][]);
    }
}