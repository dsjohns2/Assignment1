package com.company;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * The class that runs the game
 */
public class Main {
    public static void main(String[] args) throws InterruptedException, IOException {
        Controller control = new Controller();
        control.init();
    }
}
