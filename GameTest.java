package com.company;

import static org.junit.Assert.*;

/**
 * This is the JUnit4 tests for the program
 */
public class GameTest {
    @org.junit.Test
    public void get_piece_name() {
        Game test_game = new Game();
        assertEquals (test_game.get_piece_name(1, 6), "P");
        assertEquals (test_game.get_piece_name(0, 4), "K");
        assertEquals (test_game.get_piece_name(7, 6), "N");
    }

    @org.junit.Test
    public void make_move() throws InterruptedException {
        Game test_game = new Game();
        test_game.initialize_board();
        assertEquals (test_game.make_move(), -1);
    }

    @org.junit.Test
    public void initialize_board() {

    }

    @org.junit.Test
    public void add_piece_to_location() {
    }
    @org.junit.Test
    public void print_board() {
    }

    @org.junit.Test
    public void check_for_end_of_game() {
    }

    @org.junit.Test
    public void pick_move() {
    }

    @org.junit.Test
    public void get_all_moves() {
    }

    @org.junit.Test
    public void get_legal_moves() {
    }

    @org.junit.Test
    public void remove_moves_into_check() {
    }

}