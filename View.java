package com.company;


import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;

//based off of code at https://www.javatpoint.com/java-jpanel
import java.awt.*;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Vector;
import javax.swing.*;

/**
 * This is the View class in the MCV model
 */
public class View {
    public JFrame f;
    private JButton buttons[][];
    private JPanel panel;
    private JPanel control_panel;
    private JPanel name_panel;
    private JButton white_forfeit;
    private JButton black_forfeit;
    private JButton white_draw;
    private JButton black_draw;
    private JButton score;
    private JButton undo;
    /**
     * This is the default constructor that takes in the controller
     * @param controller this is the controller of the view
     * @throws IOException
     */
    View(Controller controller) throws IOException {
        Game game = controller.my_game;
        f= new JFrame("Chess");
        panel=new JPanel();
        panel.setLayout(new GridLayout(8, 8, 0, 0));
        panel.setBounds(0,0,500,500);
        panel.setBackground(Color.gray);
        buttons = new JButton[8][8];
        for(int i=0; i<8; i++) {
            for (int j = 0; j<8; j++) {
                JButton button = new JButton("");
                button.setSize(40, 40);
                if((i+j) % 2 == 0) {
                    button.setBackground(Color.lightGray);
                }
                else{
                    button.setBackground(Color.gray);
                }
                boolean occupied = game.board[i][j].occupied;
                String cur_piece = game.board[i][j].name;
                boolean white = game.board[i][j].white;
                if(occupied) {
                    Image image;
                    if (cur_piece == "P" && !white) {
                        image = ImageIO.read(getClass().getResource("b_pawn.png"));
                    }
                    else if (cur_piece == "R" && !white) {
                        image = ImageIO.read(getClass().getResource("b_rook.png"));
                    }
                    else if (cur_piece == "N" && !white) {
                        image = ImageIO.read(getClass().getResource("b_knight.png"));
                    }
                    else if (cur_piece == "B" && !white) {
                        image = ImageIO.read(getClass().getResource("b_bishop.png"));
                    }
                    else if (cur_piece == "Q" && !white) {
                        image = ImageIO.read(getClass().getResource("b_queen.png"));
                    }
                    else if (cur_piece == "K" && !white) {
                        image = ImageIO.read(getClass().getResource("b_king.png"));
                    }
                    else if (cur_piece == "A" && !white) {
                        image = ImageIO.read(getClass().getResource("b_spy.png"));
                    }
                    else if (cur_piece == "Z" && !white) {
                        image = ImageIO.read(getClass().getResource("b_joker.png"));
                    }
                    else if (cur_piece == "P" && white) {
                        image = ImageIO.read(getClass().getResource("w_pawn.png"));
                    }
                    else if (cur_piece == "R" && white) {
                        image = ImageIO.read(getClass().getResource("w_rook.png"));
                    }
                    else if (cur_piece == "N" && white) {
                        image = ImageIO.read(getClass().getResource("w_knight.png"));
                    }
                    else if (cur_piece == "B" && white) {
                        image = ImageIO.read(getClass().getResource("w_bishop.png"));
                    }
                    else if (cur_piece == "Q" && white) {
                        image = ImageIO.read(getClass().getResource("w_queen.png"));
                    }
                    else if (cur_piece == "A" && white) {
                        image = ImageIO.read(getClass().getResource("w_spy.png"));
                    }
                    else if (cur_piece == "Z" && white) {
                        image = ImageIO.read(getClass().getResource("w_joker.png"));
                    }
                    else {
                        image = ImageIO.read(getClass().getResource("w_king.png"));
                    }
                    Image newimage = image.getScaledInstance(35, 35, java.awt.Image.SCALE_SMOOTH);
                    button.setIcon(new ImageIcon(newimage));
                }
                panel.add(button);
                buttons[i][j] = button;
            }
        }
        name_panel = new JPanel();
        name_panel.setLayout(new GridLayout(1, 3, 0, 0));
        name_panel.setBounds(0,500,500,20);
        name_panel.setBackground(Color.gray);
        JButton white_name = new JButton(controller.player1);
        JButton black_name = new JButton(controller.player2);
        score = new JButton(game.white_score + " - " + game.black_score);
        name_panel.add(white_name);
        name_panel.add(score);
        name_panel.add(black_name);
        control_panel=new JPanel();
        control_panel.setLayout(new GridLayout(1, 5, 0, 0));
        control_panel.setBounds(0,520,500,40);
        control_panel.setBackground(Color.gray);
        white_forfeit = new JButton("Forfeit");
        black_forfeit = new JButton("Forfeit");
        white_draw = new JButton("Draw");
        black_draw = new JButton("Draw");
        undo = new JButton("Undo");
        white_forfeit.setBackground(Color.white);
        white_draw.setBackground(Color.white);
        black_draw.setBackground(Color.darkGray);
        black_forfeit.setBackground(Color.darkGray);
        control_panel.add(white_forfeit);
        control_panel.add(white_draw);
        control_panel.add(undo);
        control_panel.add(black_draw);
        control_panel.add(black_forfeit);
        f.add(panel);
        f.add(control_panel);
        f.add(name_panel);
        f.setSize(600,600);
        f.setLayout(null);
        f.setVisible(true);
    }
    public void update_buttons(Game game, JPanel panel) throws IOException {
        score.setText(game.white_score + " - " + game.black_score);
        for(int i=0; i<8; i++) {
            for (int j = 0; j<8; j++) {
                JButton button = this.buttons[i][j];
                boolean occupied = game.board[i][j].occupied;
                String cur_piece = game.board[i][j].name;
                boolean white = game.board[i][j].white;
                if(occupied) {
                    Image image;
                    if (cur_piece == "P" && !white) {
                        image = ImageIO.read(getClass().getResource("b_pawn.png"));
                    }
                    else if (cur_piece == "R" && !white) {
                        image = ImageIO.read(getClass().getResource("b_rook.png"));
                    }
                    else if (cur_piece == "N" && !white) {
                        image = ImageIO.read(getClass().getResource("b_knight.png"));
                    }
                    else if (cur_piece == "B" && !white) {
                        image = ImageIO.read(getClass().getResource("b_bishop.png"));
                    }
                    else if (cur_piece == "Q" && !white) {
                        image = ImageIO.read(getClass().getResource("b_queen.png"));
                    }
                    else if (cur_piece == "K" && !white) {
                        image = ImageIO.read(getClass().getResource("b_king.png"));
                    }
                    else if (cur_piece == "A" && !white) {
                        image = ImageIO.read(getClass().getResource("b_spy.png"));
                    }
                    else if (cur_piece == "Z" && !white) {
                        image = ImageIO.read(getClass().getResource("b_joker.png"));
                    }
                    else if (cur_piece == "P" && white) {
                        image = ImageIO.read(getClass().getResource("w_pawn.png"));
                    }
                    else if (cur_piece == "R" && white) {
                        image = ImageIO.read(getClass().getResource("w_rook.png"));
                    }
                    else if (cur_piece == "N" && white) {
                        image = ImageIO.read(getClass().getResource("w_knight.png"));
                    }
                    else if (cur_piece == "B" && white) {
                        image = ImageIO.read(getClass().getResource("w_bishop.png"));
                    }
                    else if (cur_piece == "Q" && white) {
                        image = ImageIO.read(getClass().getResource("w_queen.png"));
                    }
                    else if (cur_piece == "A" && white) {
                        image = ImageIO.read(getClass().getResource("w_spy.png"));
                    }
                    else if (cur_piece == "Z" && white) {
                        image = ImageIO.read(getClass().getResource("w_joker.png"));
                    }
                    else {
                        image = ImageIO.read(getClass().getResource("w_king.png"));
                    }
                    Image newimage = image.getScaledInstance(35, 35, java.awt.Image.SCALE_SMOOTH);
                    button.setIcon(new ImageIcon(newimage));
                }
                else{
                    button.setIcon(new ImageIcon());
                }
            }
        }
    }
    public void update_view(Game my_game) throws IOException {
        update_buttons(my_game, this.panel);
    }
    public void undo_press(ActionListener a){
        undo.addActionListener(a);
    }
    public void white_forfeits(ActionListener a){
        white_forfeit.addActionListener(a);
    }
    public void black_forfeits(ActionListener a){
        black_forfeit.addActionListener(a);
    }
    public void white_draws(ActionListener a){
        white_draw.addActionListener(a);
    }
    public void black_draws(ActionListener a){
        black_draw.addActionListener(a);
    }
    public void square_move(ActionListener a){
        for(int i=0; i<8; i++) {
            for (int j = 0; j<8; j++) {
                buttons[i][j].addActionListener(a);
            }
        }
    }
}
