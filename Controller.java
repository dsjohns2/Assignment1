package com.company;

import javax.swing.*;
import java.applet.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * This is the Controller class in the MCV model
 */
public class Controller extends Applet {
    public Game my_game;
    public View my_view;
    public boolean piece_selected = false;
    public JButton first_clicked;
    public int first_clicked_x;
    public int first_clicked_y;
    public Color first_clicked_color;
    public JButton second_clicked;
    public int second_clicked_x;
    public int second_clicked_y;
    public String player1;
    public String player2;
    public boolean draw_offered = false;

    public void init(){
        try {
            my_game = new Game();
            my_game.initialize_board();
            player1 = JOptionPane.showInputDialog("What is Player 1's name?");
            player2 = JOptionPane.showInputDialog("What is Player 2's name?");
            my_view = new View(this);

            my_view.undo_press(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    for(int i=0; i<8; i++) {
                        for (int j = 0; j < 8; j++) {
                            my_game.board[i][j] = new Square(my_game.last_position[i][j]);
                        }
                    }
                    my_game.white_turn = !my_game.white_turn;
                    try {
                        my_view.update_view(my_game);
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
            });

            my_view.white_draws(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if(draw_offered){
                        JOptionPane.showMessageDialog(my_view.f, "Draw");
                        my_game.initialize_board();
                        draw_offered = false;
                    }
                    else{
                        draw_offered = true;
                    }
                    try {
                        my_view.update_view(my_game);
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
            });

            my_view.black_draws(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if(draw_offered){
                        JOptionPane.showMessageDialog(my_view.f, "Draw");
                        my_game.initialize_board();
                        draw_offered = false;
                    }
                    else{
                        draw_offered = true;
                    }
                    try {
                        my_view.update_view(my_game);
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
            });

            my_view.white_forfeits(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    my_game.initialize_board();
                    my_game.black_score += 1;
                    try {
                        my_view.update_view(my_game);
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
            });
            my_view.black_forfeits(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    my_game.initialize_board();
                    my_game.white_score += 1;
                    try {
                        my_view.update_view(my_game);
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
            });
            my_view.square_move(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if(!piece_selected) {
                        first_clicked = (JButton) e.getSource();
                        first_clicked_y = (first_clicked.getBounds().x-2)/62;
                        first_clicked_x = (first_clicked.getBounds().y-2)/62;
                        first_clicked_color = first_clicked.getBackground();
                        first_clicked.setBackground(Color.yellow);
                        piece_selected = true;
                    }
                    else {
                        first_clicked.setBackground(first_clicked_color);
                        second_clicked = (JButton) e.getSource();
                        second_clicked_y = (second_clicked.getBounds().x-2)/62;
                        second_clicked_x = (second_clicked.getBounds().y-2)/62;
                        Game potential_move = new Game(my_game);
                        potential_move.add_piece_to_location(second_clicked_x, second_clicked_y, my_game.board[first_clicked_x][first_clicked_y].name, my_game.white_turn, true);
                        potential_move.add_piece_to_location(first_clicked_x, first_clicked_y, "", true, false);
                        boolean move_found = false;
                        Square[][][] legal_moves = my_game.get_all_moves(my_game.white_turn, my_game.board);
                        for(int i=0; i<legal_moves.length; i++){
                            if(my_game.equal_boards(legal_moves[i], potential_move.board)){
                                move_found = true;
                                break;
                            }
                        }
                        if(move_found){
                            for(int i=0; i<8; i++) {
                                for (int j = 0; j < 8; j++) {
                                    my_game.last_position[i][j] = new Square(my_game.board[i][j]);
                                }
                            }
                            my_game.add_piece_to_location(second_clicked_x, second_clicked_y, my_game.board[first_clicked_x][first_clicked_y].name, my_game.white_turn, true);
                            my_game.add_piece_to_location(first_clicked_x, first_clicked_y, "", true, false);
                            my_game.white_turn = !my_game.white_turn;
                        }
                        else{
                            JOptionPane.showMessageDialog(my_view.f, "Illegal");
                        }
                        try {
                            my_view.update_view(my_game);
                            legal_moves = my_game.get_all_moves(my_game.white_turn, my_game.board);
                            int end_val = my_game.check_for_end_of_game(legal_moves);
                            if(end_val == 0){
                                JOptionPane.showMessageDialog(my_view.f, "Stalemate");
                                my_game.initialize_board();
                            }
                            else if(end_val == 1){
                                JOptionPane.showMessageDialog(my_view.f, "White Wins");
                                my_game.white_score += 1;
                                my_game.initialize_board();
                            }
                            else if(end_val == 2){
                                JOptionPane.showMessageDialog(my_view.f, "Black Wins");
                                my_game.black_score += 1;
                                my_game.initialize_board();
                            }
                            my_view.update_view(my_game);
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        }
                        piece_selected = false;
                    }
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /**
     * A method to run through the game by taking turns
     * @return End game state integer
     */
    public int run_game() throws InterruptedException, IOException {
        int end_of_game = -1;
        while(end_of_game == -1){
            end_of_game = my_game.make_move();
            my_view.update_view(my_game);
            TimeUnit.SECONDS.sleep(2);
        }
        if(end_of_game == 0){
            System.out.println("Stalemate");
        }
        else if(end_of_game == 1){
            System.out.println("White Wins");
        }
        else{
            System.out.println("Black Wins");
        }

        return end_of_game;
    }
}
